import { useField } from "formik";
import { FormGroup, Input, Label } from "reactstrap";
const CustomInput = ({ label, ...props }) => {
    const [field, meta] = useField(props);
    return (
      <FormGroup>
        <Label htmlFor={props.id || props.name}>{label}</Label>
        <Input {...field} {...props} />
        {meta.touched && meta.error ? (
          <div className="error">{meta.error}</div>
        ) : null}
      </FormGroup>
    );
};
export default CustomInput;