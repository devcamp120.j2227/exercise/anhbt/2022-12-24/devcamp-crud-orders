import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Order from './features/order/Order';

function App() {
  return (
    <div>
        <Order />
    </div>
  );
}

export default App;
