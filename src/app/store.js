import { configureStore } from '@reduxjs/toolkit';
import ordersReducer from '../features/order/orderSlice';
export const store = configureStore({
  reducer: {
    orders: ordersReducer
  },
});
