import React, { useState } from 'react'
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap'

function DeleteOrder(props) {
    const orderId = props.orderId;
    const [modalOpen, setModalOpen] = useState(false);
    const toggle = () => setModalOpen(!modalOpen);

    const deleteOrder = async (data) => {
        var requestOptions = {
        method: 'DELETE',
        redirect: 'follow'
        };
    
        await fetch(`http://203.171.20.210:8080/devcamp-pizza365/orders/${orderId}`, requestOptions)
        .then(response => response.text())
        .then(alert(`xoá order ${orderId}`))

        window.location.reload();
}
return (
    <div>
        <Button color="danger" onClick={toggle}>
            Delete Order
        </Button>
        <Modal isOpen={modalOpen} toggle={toggle}>
            <ModalHeader toggle={toggle}>Delete Order</ModalHeader>
            <ModalBody>
                Xoá Order này? ID: {orderId}
            </ModalBody>
            <ModalFooter>
              <Button color="danger" onClick={deleteOrder}>
                Confirm
              </Button>
              <Button color="secondary" onClick={toggle}>
                Cancel
              </Button>
            </ModalFooter>
        </Modal>
    </div>
)}

export default DeleteOrder