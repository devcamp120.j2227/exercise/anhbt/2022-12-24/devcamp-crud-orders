import React, { useEffect, useState } from 'react';
import ReactPaginate from 'react-paginate';
import { useSelector } from 'react-redux';
import { ButtonGroup, Container, Table } from 'reactstrap';
import CreateOrder from './CreateOrder';
import DeleteOrder from './DeleteOrder';
import UpdateOrder from './UpdateOrder';

export default function Order() {
  const orders = useSelector((state) => state.orders.orders);
  const ordersPerPage = 10;

  function DisplayOrder({ currentOrders }) {
    return (
      <Table hover>
          <thead>
            <tr>
              <th>id</th>
              <th>kichCo</th>
              <th>loaiPizza</th>
              <th>idVourcher</th>
              <th>idLoaiNuocUong</th>
              <th>hoTen</th>
              <th>email</th>
              <th>soDienThoai</th>
              <th>diaChi</th>
              <th>loiNhan</th>
              <th>trangThai</th>
              <th>action</th>
            </tr>
          </thead>
          <tbody>
            {currentOrders && currentOrders.map((order, index) => (
              <tr key={index}>
                <td>{order.id}</td>
                <td>{order.kichCo}</td>
                <td>{order.loaiPizza}</td>
                <td>{order.idVourcher}</td>
                <td>{order.idLoaiNuocUong}</td>
                <td>{order.hoTen}</td>
                <td>{order.email}</td>
                <td>{order.soDienThoai}</td>
                <td>{order.diaChi}</td>
                <td>{order.loiNhan}</td>
                <td>{order.trangThai}</td>
                <td><ButtonGroup>
                  <UpdateOrder orderId={order.id}/>
                  <DeleteOrder orderId={order.id}/>
                </ButtonGroup>
                </td>
              </tr>
            ))}
          </tbody>
      </Table>
    );
  }
  
  function PaginatedOrders({ ordersPerPage }) {
    const [currentOrders, setCurrentOrders] = useState(null);
    const [pageCount, setPageCount] = useState(0);
    const [orderOffset, setOrderOffset] = useState(0);
  
    useEffect(() => {
      const endOffset = orderOffset + ordersPerPage;
      setCurrentOrders(orders.slice(orderOffset, endOffset));
      setPageCount(Math.ceil(orders.length / ordersPerPage));
    }, [orderOffset, ordersPerPage]);
  
    const handlePageClick = (event) => {
      const newOffset = event.selected * ordersPerPage % orders.length;
      setOrderOffset(newOffset);
    };
  
    return (
      <>
        <DisplayOrder currentOrders={currentOrders} />
        <ReactPaginate
          nextLabel=">"
          previousLabel="<"
          onPageChange={handlePageClick}
          pageRangeDisplayed={3}
          marginPagesDisplayed={2}
          pageCount={pageCount}
          pageClassName="page-order"
          pageLinkClassName="page-link"
          previousClassName="page-order"
          previousLinkClassName="page-link"
          nextClassName="page-order"
          nextLinkClassName="page-link"
          breakLabel="..."
          breakClassName="page-order"
          breakLinkClassName="page-link"
          containerClassName="pagination"
          activeClassName="active"
          renderOnZeroPageCount={null}
        />
      </>
    );
  }
  return (
    <Container>
      <CreateOrder></CreateOrder>
      <PaginatedOrders ordersPerPage={ordersPerPage}/>
    </Container>
  )
}
