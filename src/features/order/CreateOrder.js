import { Formik, Form, useFormikContext } from 'formik';
import React, { useEffect, useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import * as Yup from 'yup'
import CustomInput from '../../components/CustomInput';


const UpdateCombo = () => {
  const { values, setFieldValue } = useFormikContext();
  useEffect(()=>{
    switch (values.kichCo) {
      case 'S':
        setFieldValue ('duongKinh', '20');
        setFieldValue ('suon', '2');
        setFieldValue ('salad', '200');
        setFieldValue ('soLuongNuoc', '2');
        setFieldValue ('thanhTien', '150000');
        break;
      case 'M':
        setFieldValue ('duongKinh', '25');
        setFieldValue ('suon', '4');
        setFieldValue ('salad', '300');
        setFieldValue ('soLuongNuoc', '3');
        setFieldValue ('thanhTien', '200000');
        break;
      case 'L':
        setFieldValue ('duongKinh', '30');
        setFieldValue ('suon', '8');
        setFieldValue ('salad', '500');
        setFieldValue ('soLuongNuoc', '4');
        setFieldValue ('thanhTien', '250000');
        break;
      default:
        break;
    }
  }, [values.kichCo, setFieldValue]);
  return null;
}

const UpdateDiscount = () => {
  const { values, setFieldValue } = useFormikContext();
  useEffect(()=>{
    var requestOptions = {
      method: 'GET',
      redirect: 'follow'
      };
      if (values.idVourcher) {
        fetch(`http://203.171.20.210:8080/devcamp-pizza365/vouchers/${values.idVourcher}`, requestOptions)
        .then(response => response.json())
        .then(result => { 
          if( result.discount>0 && result.discount <= 100 ) {
            setFieldValue('giamGia', values.thanhTien * result.discount/100 )
          } else {
            setFieldValue('giamGia', 0 )

          }
        })
        .catch(error => console.log('error', error))
      }
  }, [values.idVourcher, values.thanhTien, setFieldValue])

  return null;

}

function CreateOrder() {
  const [modalOpen, setModalOpen] = useState(false);
  const [drinks, setDrinks] = useState([]);
  const toggle = () => setModalOpen(!modalOpen);
  
  const postOrder = async (data) => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: JSON.stringify(data),
    redirect: 'follow'
    };

    await fetch("http://203.171.20.210:8080/devcamp-pizza365/orders", requestOptions)
    .then(response => response.text())
    .then(result => alert(result))

    window.location.reload()

  }
  useEffect(() => {
    fetch("http://203.171.20.210:8080/devcamp-pizza365/drinks", {
    method: 'GET',
    redirect: 'follow'
    })
    .then(response => response.json())
    .then(result => setDrinks(result))
  })

  return (
    <div>
      <Button color="success" onClick={toggle}>
        + Create Order
      </Button>
      <Modal isOpen={modalOpen} toggle={toggle}>
        <Formik
          initialValues={ {
            kichCo: '',
            duongKinh: '',
            suon: '',
            salad: '',
            soLuongNuoc: '',
            thanhTien: '',
            giamGia: '',
            loaiPizza: '',
            idVourcher: '',
            idLoaiNuocUong: '',
            hoTen: '',
            email: '',
            soDienThoai: '',
            diaChi: '',
            loiNhan: '',
          }}
          validationSchema= {Yup.object({
            kichCo: Yup.string().required('required'),
            loaiPizza: Yup.string().required('required'),
            idLoaiNuocUong: Yup.string().required('required'),
            hoTen: Yup.string().required('required'),
            soDienThoai: Yup.string().required('required'),
            diaChi: Yup.string().required('required'),
          })}
          onSubmit = {values => {
            postOrder(values);
          }}>
          <Form>
            <ModalHeader toggle={toggle}>Create Order</ModalHeader>
            <ModalBody>
              <CustomInput type='select' label="kichCo (*)" name="kichCo">
                <option value='' disabled>...</option>
                <option value='S'>S</option>
                <option value='M'>M</option>
                <option value='L'>L</option>
              </CustomInput>
              <CustomInput
                label="duongKinh"
                name="duongKinh"
                type="text"
                disabled
              />
              <CustomInput
                label="suon"
                name="suon"
                type="text"
                disabled
              />
              <CustomInput
                label="salad"
                name="salad"
                type="text"
                disabled
              />
              <CustomInput
                label="soLuongNuoc"
                name="soLuongNuoc"
                type="text"
                disabled
              />
              <CustomInput
                label="thanhTien"
                name="thanhTien"
                type="text"
                disabled
              />
              <CustomInput
                label="giamGia"
                name="giamGia"
                type="text"
                disabled
              />
              <UpdateDiscount/>
              <UpdateCombo/>
              <CustomInput type='select' label="loaiPizza (*)" name="loaiPizza">
                <option value="" disabled>...</option>
                <option value="Seafood">Hải sản</option>
                <option value="Hawaii">Hawaii</option>
                <option value="Bacon">Thịt hun khói</option>
              </CustomInput>
              <CustomInput
                label="idVourcher (numbers only)"
                name="idVourcher"
                type="text"
                pattern="^[0-9]+$"
              />
              <CustomInput type='select' label="idLoaiNuocUong (*)" name="idLoaiNuocUong">
                <option value="" disabled>...</option>
                {drinks.map((drink, index) => {
                  return <option key={index} value={drink.maNuocUong}>{drink.tenNuocUong}</option>
                })}
              </CustomInput>
              <CustomInput
                label="hoTen (*)"
                name="hoTen"
                type="text"
              />
              <CustomInput
                label="email"
                name="email"
                type="email"
              />
              <CustomInput
                label="soDienThoai (*)"
                name="soDienThoai"
                type="text"
                pattern="^[0-9]+$"
              />
              <CustomInput
                label="diaChi (*)"
                name="diaChi"
                type="text"
              />
              <CustomInput
                label="loiNhan"
                name="loiNhan"
                type="text"
              />
            </ModalBody>
            <ModalFooter>
              <Button color="primary" type='submit'>
                Submit
              </Button>
              <Button color="secondary" onClick={toggle}>
                Cancel
              </Button>
            </ModalFooter>
          </Form>
        </Formik>
      </Modal>
    </div>
  );
}

export default CreateOrder;