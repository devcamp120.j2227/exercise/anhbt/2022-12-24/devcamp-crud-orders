import { Form, Formik } from 'formik';
import React, { useState } from 'react'
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap'
import CustomInput from '../../components/CustomInput';

function UpdateOrder(props) {
    const orderId = props.orderId;
    const [modalOpen, setModalOpen] = useState(false);
    const toggle = () => setModalOpen(!modalOpen);

    const putOrder = async (data) => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
    
        var requestOptions = {
        method: 'PUT',
        headers: myHeaders,
        body: JSON.stringify(data),
        redirect: 'follow'
        };
    
        await fetch(`http://203.171.20.210:8080/devcamp-pizza365/orders/${orderId}`, requestOptions)
        .then(response => response.text())
        .then(result => alert( JSON.parse( result).trangThai ))

        window.location.reload();
}
return (
    <div>
        <Button color="info" onClick={toggle}>
            Update Order
        </Button>
        <Modal isOpen={modalOpen} toggle={toggle}>
        <Formik
          initialValues={ {
            trangThai: 'open',
          }}
          onSubmit = {values => {
            putOrder(values);
          }}>
          <Form>
            <ModalHeader toggle={toggle}>Update Order</ModalHeader>
            <ModalBody>
                <CustomInput type='select' label="trangThai (*)" name="trangThai">
                    <option value='open'>Open</option>
                    <option value='cancel'>Đã hủy</option>
                    <option value='confirm'>Đã xác nhận</option>
                </CustomInput>
            </ModalBody>
            <ModalFooter>
              <Button color="primary" type='submit'>
                Submit
              </Button>
              <Button color="secondary" onClick={toggle}>
                Cancel
              </Button>
            </ModalFooter>
          </Form>
        </Formik>
        </Modal>
    </div>
)}

export default UpdateOrder