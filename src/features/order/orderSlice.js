import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

export const fetchOrders = createAsyncThunk("fetchOrders", async () => {
    const response = await fetch("http://203.171.20.210:8080/devcamp-pizza365/orders");
    const orders = await response.json();
    return orders;
});

const ordersSlice = createSlice({
    name: "orders",
    initialState: {
        orders: []
    },
    reducers: {},
    extraReducers: {
        [fetchOrders.fulfilled] : (state, action) => {
            state.orders = [...state.orders, ...action.payload];
        },
    }
})

export default ordersSlice.reducer;
